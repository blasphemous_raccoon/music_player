# Generated by Django 2.2.1 on 2019-06-07 19:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("music", "0002_auto_20190607_1908")]

    operations = [
        migrations.AlterField(
            model_name="album",
            name="artist",
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to="music.Artist"),
        )
    ]
