from django.http import HttpResponseRedirect
from django.contrib import messages
from django.http import JsonResponse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.shortcuts import redirect, render
from django.conf import settings
from django.http import HttpResponse
from .models import *
from .forms import *
from django.db.models.functions import Left
import taglib
import hashlib


class IndexView(LoginRequiredMixin, generic.TemplateView):
    template_name = "music/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["artist_list"] = Artist.objects.annotate(first_initial=Left("name", 1))
        context["album_list"] = Album.objects.annotate(first_letter=Left("name", 1))
        return context

    @staticmethod
    def post(request):
        upload_data = dict()
        if "mp3_upload" in request.POST:
            if request.FILES["mp3_uploads"]:
                for file in request.FILES.getlist("mp3_uploads"):
                    song = taglib.File(file.temporary_file_path())
                    md5 = hashlib.md5(file.read()).hexdigest()
                    upload_data[file.name] = dict()

                    obj, created = Track.objects.get_or_create(md5_hash=md5)
                    if created:
                        obj.file = file
                    obj.title = song.tags["TITLE"][0]
                    obj.save()

                    upload_data[file.name]["id"] = obj.pk
                    for tag in ("album", "artist", "title"):
                        if (tag.upper() in song.tags) and song.tags[tag.upper()][0]:
                            upload_data[file.name][tag] = song.tags[tag.upper()][0]
        return render(request, "music/upload.html", {"upload_data": upload_data})


class UploadView(LoginRequiredMixin, generic.TemplateView):
    template_name = "music/upload.html"

    def post(self, request):
        for key in request.POST:
            if key.endswith(".mp3"):
                artist_name, album_name, track_title, track_id = request.POST.getlist(key)
                artist, _ = Artist.objects.get_or_create(name=artist_name)
                album, _ = Album.objects.get_or_create(name=album_name, artist_id=artist.id)
                track = Track.objects.get(pk=track_id)
                track.artist = artist
                track.album = album
                track.save()

            else:
                pass
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


class TagModifyView(LoginRequiredMixin, generic.TemplateView):
    template_name = "music/tag_modify.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["artists"] = Artist.objects.all()
        context["playlists"] = Playlist.objects.all()
        track_id = None
        album_id = None
        artist_id = None

        if "type" not in kwargs:
            context["albums"] = Album.objects.all()
            context["tracks"] = Track.objects.all()
        else:
            if kwargs["type"] == "artist":
                artist_id = kwargs["pk"]
                artist = Artist.objects.get(pk=artist_id)
                context["albums"] = Album.objects.filter(artist_id=artist_id)
                context["tracks"] = Track.objects.filter(artist_id=artist_id)
                context["form"] = ArtistForm(instance=artist)
            if kwargs["type"] == "album":
                album_id = kwargs["pk"]
                album = Album.objects.get(pk=album_id)
                context["tracks"] = Track.objects.filter(album_id=album_id)
                context["form"] = AlbumForm(instance=album)
            if kwargs["type"] == "track":
                track_id = kwargs["pk"]
                track = Track.objects.get(pk=track_id)
                context["tracks"] = Track.objects.filter(album_id=track.album_id)
                context["form"] = TrackForm(instance=track)

        context["sidebar_status"] = {
            "artists": not (artist_id or album_id or track_id),
            "albums": bool(artist_id),
            "tracks": album_id or track_id,
        }
        context["playlist_form"] = PlaylistForm()
        context["playlists"] = PlaylistTracksForm(user_id=self.request.user.id)
        return context

    def post(self, request, type, pk):
        if "tag_modify" in request.POST:
            if type == "artist":
                form = ArtistForm(request.POST, instance=Artist.objects.get(pk=pk))
            elif type == "album":
                form = AlbumForm(request.POST, instance=Album.objects.get(pk=pk))
            elif type == "track":
                form = TrackForm(request.POST, instance=Track.objects.get(pk=pk))
            form.save()
            messages.success(request, "Tags were successfully updated")
        elif "delete" in request.POST:
            if type == "artist":
                Artist.objects.get(pk=pk).delete()
                return HttpResponseRedirect("/tag_modify/")
            elif type == "album":
                artist_id = str(Album.objects.get(pk=pk).artist_id)
                Album.objects.get(pk=pk).delete()
                return HttpResponseRedirect("/tag_modify/artist/" + artist_id)
            elif type == "track":
                album_id = str(Track.objects.get(pk=pk).album_id)
                Track.objects.get(pk=pk).delete()
                return HttpResponseRedirect("/tag_modify/album/" + album_id)
            messages.success(request, "Item was successfully deleted")
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


class AddToPlaylistView(TagModifyView):
    template_name = "music/playlist_modify.html"

    def post(self, request, type=None, pk=None):
        if "create_playlist" in request.POST:
            obj = Playlist.objects.create(name=request.POST["name"], user_id=request.user.id)
            obj.save()
            messages.success(request, "Playlist was successfully created")
        elif "delete_playlist" in request.POST:
            Playlist.objects.get(pk=request.POST["playlist"]).delete()
            messages.success(request, "Playlist was successfully deleted")
        elif "add_to_playlist" in request.POST:
            success_tracks = 0
            fail_tracks = 0
            if "tracks_id" not in request.POST:
                messages.error(request, "You must choose at least one track")
            else:
                for track_id in request.POST.getlist("tracks_id"):
                    try:
                        track_num = PlaylistTracks.objects.get(
                            playlist_id=request.POST["playlist"], track_id=track_id
                        ).track_num
                    except PlaylistTracks.DoesNotExist:
                        track_num = None
                    if not track_num:
                        obj = PlaylistTracks.objects.create(playlist_id=request.POST["playlist"], track_id=track_id)
                        obj.save()
                        success_tracks += 1
                    else:
                        fail_tracks += 1
                if success_tracks:
                    if success_tracks == 1:
                        messages.success(request, "1 track was added to playlist")
                    else:
                        messages.success(request, str(success_tracks) + " tracks were added to playlist")

                if fail_tracks:
                    if fail_tracks == 1:
                        messages.error(request, "1 track is already in the list")
                    else:
                        messages.error(request, str(fail_tracks) + " tracks are already in the list")

        return HttpResponseRedirect("#")


class PlaylistView(TagModifyView):
    template_name = "music/playlist.html"


class PlaylistTracksView(View):
    def get(self, request, pk):
        track_list = PlaylistTracks.objects.filter(playlist_id=pk).order_by("track_num")
        return JsonResponse(
            [
                {
                    "title": f"{track.artist} ({track.album}) - {track.title}",
                    "file": settings.MEDIA_URL + track.file.name,
                }
                for track in Track.objects.filter(id__in=[i.track_id for i in track_list])
            ],
            safe=False,
        )
