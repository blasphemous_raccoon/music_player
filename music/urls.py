from django.urls import path
from django.contrib.auth.views import LogoutView
from django.contrib.auth.views import LoginView
from . import views


urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("tag_modify/<str:type>/<int:pk>/", views.TagModifyView.as_view(), name="tag_modify"),
    path("tag_modify/", views.TagModifyView.as_view(), name="tag_modify"),
    path("upload", views.UploadView.as_view(), name="upload"),
    path("playlist_modify/<str:type>/<int:pk>/", views.AddToPlaylistView.as_view(), name="playlist_modify"),
    path("playlist_modify/", views.AddToPlaylistView.as_view(), name="playlist_modify"),
    path("playlist", views.PlaylistView.as_view(), name="playlist"),
    path("playlist/<int:pk>", views.PlaylistTracksView.as_view(), name="playlist_tracks"),
]
