from django import forms
from .models import *


class TrackForm(forms.ModelForm):
    class Meta:
        model = Track
        fields = ["artist", "title", "album"]


class ArtistForm(forms.ModelForm):
    class Meta:
        model = Artist
        fields = "__all__"


class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = "__all__"


class PlaylistForm(forms.ModelForm):
    class Meta:
        model = Playlist
        fields = ["name"]


class TracksListForm(forms.Form):
    tracks_id = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)


class PlaylistTracksForm(forms.ModelForm):
    class Meta:
        model = PlaylistTracks
        fields = ["playlist"]

    def __init__(self, user_id=None, **kwargs):
        super(PlaylistTracksForm, self).__init__(**kwargs)
        if user_id:
            self.fields["playlist"].queryset = Playlist.objects.filter(user_id=user_id)
