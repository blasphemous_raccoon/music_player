from django.db import models
from django.conf import settings


class Track(models.Model):
    class Meta:
        verbose_name = "Music Track"
        verbose_name_plural = "Music Tracks"

    artist = models.ForeignKey("Artist", on_delete=models.PROTECT, null=True)
    title = models.CharField(max_length=1024, null=True)
    album = models.ForeignKey("Album", on_delete=models.PROTECT, null=True)
    file = models.FileField()
    md5_hash = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.artist.name} - {self.title} ({self.album.name})"


class Artist(models.Model):
    name = models.CharField(max_length=1024)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Album(models.Model):
    artist = models.ForeignKey("Artist", on_delete=models.PROTECT, null=True)
    name = models.CharField(max_length=1024)
    description = models.TextField(null=True, blank=True)
    cover = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name


class Playlist(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Playlist name")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


def default_track_num():
    try:
        return max([i.track_num for i in PlaylistTracks.objects.all()]) + 1
    except ValueError:
        return 0


class PlaylistTracks(models.Model):
    class Meta:
        unique_together = ("playlist", "track")

    playlist = models.ForeignKey("Playlist", on_delete=models.CASCADE, null=True)
    track = models.ForeignKey("Track", on_delete=models.CASCADE, null=True)
    track_num = models.IntegerField(default=default_track_num, db_index=True)

    def __str__(self):
        return str(self.track_num)
