# We Use an official Python runtime as a parent image
FROM python:3.6
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1
# create root directory for our project in the container
RUN apt-get update && apt-get install -y libtag-extras1 libtag-extras-dev
RUN mkdir /music_player
WORKDIR /music_player
ADD requirements.txt /music_player
RUN pip install -r requirements.txt
ADD . /music_player/
RUN python manage.py collectstatic --noinput
EXPOSE 8000
CMD python manage.py migrate && exec gunicorn music_player.wsgi:application --bind 0.0.0.0:8000 --workers 3
